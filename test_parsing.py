import json
import glob
import os 
import sys
import csv

#NOTES: 
#usage command line: python json_conll.py input_folder

def get_paths(input_folder):
    """
    Stores all .txt files in a list
    Returns a list of strings of filepaths from the Text (volumes) folder
    :param inputfolder: inputfolder used in main
    """
    list_files = []
    conll_folder = glob.glob(input_folder + '/*.json')
    
    for filename in conll_folder:
        list_files.append(filename)

    return list_files

def load_text(txt_path):
    """
    Opens the container en reads the elements(strings)
    Returns a string
    :param txt_path: list with filepaths
    """
    with open(txt_path, 'r') as json_file:
        data = json_file.read()
        content = json.loads(data)
            
    return content

def main():
    
    input_folder = sys.argv[1] 
    print(input_folder)
    txt_path = get_paths(input_folder)
    for text in txt_path:
        print(f"START   PARSING {text}")
        loaded_dicts = load_text(text)
        print(f"SUCCESS PARSING {text}")
            
if __name__ == "__main__":
    main()