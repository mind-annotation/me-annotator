FROM node:12-alpine3.12

WORKDIR '/app'
COPY . .
RUN apk add python3
RUN npm install